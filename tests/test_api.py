"""
Test the HTTP API itself with a dummy client.
"""
import io
import json
import requests
import pytest
import starlette.testclient
import api.main
from tests.helpers import setup

client = starlette.testclient.TestClient(api.main.app)


@pytest.fixture
def expected_rate():
    return 0.804


@pytest.fixture
def exchange_rates_api_fixed_dict(expected_rate) -> dict:
    return {"rates":{"EUR":expected_rate},"base":"USD","date":"2021-03-22"}


@pytest.fixture
def exchange_rate_api_com_fixed_dict(expected_rate) -> dict:
    return {'result': 'success', 'documentation': 'https://www.exchangerate-api.com/docs', 'terms_of_use': 'https://www.exchangerate-api.com/terms', 'time_last_update_unix': 1616544001, 'time_last_update_utc': 'Wed, 24 Mar 2021 00:00:01 +0000', 'time_next_update_unix': 1616630416, 'time_next_update_utc': 'Thu, 25 Mar 2021 00:00:16 +0000', 'base_code': 'USD', 'target_code': 'EUR', 'conversion_rate': expected_rate}


def test_quote_dry(exchange_rates_api_fixed_dict,
                   exchange_rate_api_com_fixed_dict,
                   expected_rate, monkeypatch, setup):
    def fixed_response(url, *_, **__):
        if "exchangerate-api.com" in url:
            json_raw = exchange_rate_api_com_fixed_dict
        else:
            json_raw = exchange_rates_api_fixed_dict
        r = requests.Response()
        r.raw = io.BytesIO(json.dumps(json_raw).encode('utf-8'))
        return r
    monkeypatch.setattr(requests, "get", fixed_response)
    uri = "/api/quote?from_currency_code=USD&to_currency_code=EUR&amount=100"
    response = client.get(uri)
    assert response.json()["exchange_rate"] == expected_rate

"""
Common functions/fixtures used in tests.
"""
import pytest
import api.cache


def mock_cache(*_, **__):
    return None


@pytest.fixture
def setup(monkeypatch):
    monkeypatch.setattr(api.cache.RedisDB, "write", mock_cache)
    monkeypatch.setattr(api.cache.RedisDB, "read", mock_cache)




"""
Test our usage of a live Redis.
Write, read and expiration of a key.

The "test_*" keys are deleted at the end of execution.
Others are expired quickly.
Even though these aren't meant to run against any production.
"""
import os
import time
import pytest
import redis
import api.cache
import api.config


@pytest.fixture(scope="module")
def url():
    return os.environ.get("TEST_REDIS", "redis://127.0.0.1")


@pytest.fixture(scope="module")
def redis_connection(url):
    """
    Provides the same connection for any test that needs it.
    Cleans up all keys prefixed with "test_" at the end.
    """
    r = redis.from_url(url)
    yield r
    for key in r.scan(match="test_*")[-1]:
        r.delete(key)


@pytest.mark.cachedb
def test_write_redis(url, redis_connection, monkeypatch):
    """
    Test writing a value using implemented abstraction stores actual value.
    """
    r = redis_connection
    key, value = "test_foo", "boo"
    monkeypatch.setattr(api.cache.RedisDB, "url", url)
    assert api.cache.RedisDB.write(key=key, value=value)
    assert r.get(key).decode(api.cache.RedisDB.encoding) == value


@pytest.mark.cachedb
def test_read_redis(url, redis_connection, monkeypatch):
    """
    Test reading, using our abstraction, a previously written value works.
    """
    r = redis_connection
    key, value = "test_foo2", "boo"
    encoding = api.cache.RedisDB.encoding
    monkeypatch.setattr(api.cache.RedisDB, "url", url)
    r.set(key.encode(encoding), value.encode(encoding))
    assert api.cache.RedisDB.read(key=key) == value


@pytest.mark.cachedb
def test_expiration(url, redis_connection, monkeypatch):
    """
    Test expiration works.
    """
    r = redis_connection
    key, value = "test_notfoo", "boo"
    encoding = api.cache.RedisDB.encoding
    monkeypatch.setattr(api.cache.RedisDB, "url", url)
    monkeypatch.setattr(api.cache.RedisDB, "expiration_msec", 5)
    assert api.cache.RedisDB.write(key=key, value=value)
    time.sleep(0.01)
    assert api.cache.RedisDB.read(key=key) != r.get(key.encode(encoding))


@pytest.mark.cachedb
def test_timed_cache(monkeypatch):
    """
    Test we can't catch Flash if he's cached.
    """
    @api.cache.timed_cache
    def cache_this():
        return time.time()

    monkeypatch.setattr(api.cache.RedisDB, "expiration_msec", 5)
    flash = cache_this()
    assert flash == cache_this()


@pytest.mark.cachedb
def test_timed_cache(monkeypatch):
    """
    Test same args, different functions cache into different keys.
    """
    @api.cache.timed_cache
    def cache_this():
        return 5

    @api.cache.timed_cache
    def cache_that():
        return 6

    monkeypatch.setattr(api.cache.RedisDB, "expiration_msec", 5)
    assert cache_this() != cache_that()

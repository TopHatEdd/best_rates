"""
Test our ability to work with different exchanges.
"""
from typing import Callable
import io
import json
import requests
import pytest
import api.exchanges
import api.cache
from tests.helpers import setup


@pytest.mark.parametrize("response,rate,method", [
    ({"rates":{"ILS":3.3040415898},"base":"USD","date":"2021-03-22"},
     api.exchanges.Rate(from_currency_code="USD",
                        to_currency_code="ILS",
                        rate=3.3040415898),
    api.exchanges.ExchangeRateAPI.response2rate),
    ({'result': 'success', 'documentation': 'https://www.exchangerate-api.com/docs', 'terms_of_use': 'https://www.exchangerate-api.com/terms', 'time_last_update_unix': 1616544001, 'time_last_update_utc': 'Wed, 24 Mar 2021 00:00:01 +0000', 'time_next_update_unix': 1616630416, 'time_next_update_utc': 'Thu, 25 Mar 2021 00:00:16 +0000', 'base_code': 'EUR', 'target_code': 'GBP', 'conversion_rate': 0.8626},
     api.exchanges.Rate(from_currency_code="EUR",
                        to_currency_code="GBP",
                        rate=0.8626),
    api.exchanges.ExchangeRateAPICom.response2rate),
])
def test_response2rate(response: dict, rate: api.exchanges.Rate, method: Callable):
    """
    Parsing expected responses works.
    """
    got = method(response=response)
    assert got == rate


@pytest.mark.parametrize("response,rate,function", [
    ({"rates":{"ILS":3.3040415898},"base":"USD","date":"2021-03-22"},
     api.exchanges.Rate(from_currency_code="USD",
                        to_currency_code="ILS",
                        rate=3.3040415898),
     api.exchanges.exchange_rates_api),
    ({'result': 'success', 'documentation': 'https://www.exchangerate-api.com/docs', 'terms_of_use': 'https://www.exchangerate-api.com/terms', 'time_last_update_unix': 1616544001, 'time_last_update_utc': 'Wed, 24 Mar 2021 00:00:01 +0000', 'time_next_update_unix': 1616630416, 'time_next_update_utc': 'Thu, 25 Mar 2021 00:00:16 +0000', 'base_code': 'EUR', 'target_code': 'GBP', 'conversion_rate': 0.8626},
     api.exchanges.Rate(from_currency_code="EUR",
                        to_currency_code="GBP",
                        rate=0.8626),
     api.exchanges.exchangerate_api_com),
])
def test_exchangeratesapi_dry(response: dict, rate: api.exchanges.Rate,
                              function: Callable, monkeypatch, setup):
    """
    Offline test using the exchangeratesapi.io API.
    """
    def fixed_response(*_, **__):
        r = requests.Response()
        r.raw = io.BytesIO(json.dumps(response).encode('utf-8'))
        return r
    monkeypatch.setattr(requests, "get", fixed_response)
    got = function(
        from_currency_code=rate.from_currency_code,
        to_currency_code=rate.to_currency_code)
    assert got == rate.rate


# We might've wrapped this in a parametrize, too, but these are important
# enough to warrant very clear intention.
@pytest.mark.expensive
def test_exchangeratesapi(setup):
    """
    Verify public APIs response structure is as expected.
    """
    rate = api.exchanges.exchange_rates_api(from_currency_code="USD",
                                            to_currency_code="ILS")


@pytest.mark.expensive
def test_exchangeratesapi(setup):
    """
    Verify public APIs response structure is as expected.
    """
    rate = api.exchanges.exchangerate_api_com(from_currency_code="USD",
                                              to_currency_code="ILS")

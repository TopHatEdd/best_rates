"""
Application wide configurations
"""
import os
import dataclasses


# dataclasses is best classes.
# Might be overkill. Although these are lighter, we aren't creating many.
# It's good to show in case someone isn't familiar with it.
@dataclasses.dataclass
class Cache:
    expiration_msec = os.environ.get("CACHE_EXPIRATION_MSEC", 10000)
    url = os.environ.get("REDIS_URL", "redis://redis")
    encoding = os.environ.get("REDIS_ENCODING", "UTF-8")


@dataclasses.dataclass
class Rates:
    precision = os.environ.get("RATE_PRECISION", 3)


@dataclasses.dataclass
class ExchangeRateAPICom:
    key = os.environ.get("EXCHANGERATE_API_COM_KEY", "")


@dataclasses.dataclass
class All:
    log_level = os.environ.get("LOG_LEVEL", "INFO")

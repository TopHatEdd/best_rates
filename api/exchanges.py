"""
Exchange[s] adapters and helper functions.
"""
import abc
import requests
import logging
import functools
import dataclasses
import requests
from . import cache
from . import config

log = logging.getLogger(__name__)


@dataclasses.dataclass
class Rate:
    from_currency_code: str
    to_currency_code: str
    rate: float


class ExchangeAdapter(metaclass=abc.ABCMeta):
    url: str

    @classmethod
    @abc.abstractmethod
    def prep_conversion_get(cls, from_currency_code: str,
                             to_currency_code: str) -> str:
        """
        Construct the GET URL.
        """
        pass

    @classmethod
    @abc.abstractmethod
    def response2rate(cls, reponse: dict) -> Rate:
        """
        Translate the response to something we can work with.
        Naively assumes responses will always be JSON.
        """
        pass


class ExchangeRateAPI(ExchangeAdapter):
    url = "https://api.exchangeratesapi.io/latest?base={base}&symbols={to}"

    @classmethod
    def prep_conversion_get(cls, from_currency_code: str,
                             to_currency_code: str) -> str:
        return cls.url.format(base=from_currency_code, to=to_currency_code)

    @classmethod
    def response2rate(cls, response: dict) -> Rate:
        try:
            from_currency_code = response["base"]
            to_currency_code, rate = next(iter(response["rates"].items()))
            return Rate(from_currency_code=from_currency_code,
                        to_currency_code=to_currency_code,
                        rate=rate)
        except (ValueError, KeyError, StopIteration) as err:
            msg = r"""Failed to parse response. Expected structure {"rates":{"ILS":3.3040415898},"base":"USD","date":"2021-03-22"}"""
            log.exception(f"{msg}. {err}")
            raise err.__class__(msg)


class ExchangeRateAPICom(ExchangeAdapter):
    url = f"https://v6.exchangerate-api.com/v6/{config.ExchangeRateAPICom.key}/pair/{{base}}/{{to}}"

    @classmethod
    def prep_conversion_get(cls, from_currency_code: str,
                             to_currency_code: str) -> str:
        return cls.url.format(base=from_currency_code, to=to_currency_code)

    @classmethod
    def response2rate(cls, response: dict) -> Rate:
        try:
            from_currency_code = response["base_code"]
            to_currency_code = response["target_code"]
            rate = response["conversion_rate"]
            return Rate(from_currency_code=from_currency_code,
                        to_currency_code=to_currency_code,
                        rate=rate)
        except (ValueError, KeyError) as err:
            msg = r"""Failed to parse response. Expected at least these keys {'base_code': 'EUR', 'target_code': 'GBP', 'conversion_rate': 0.8626}"""
            log.exception(f"{msg}. {err}")
            raise err.__class__(msg)


def fix_float(func):
    """
    Our key value stores bytes decoded to strings but we need floats.
    """
    @functools.wraps(func)
    def wrapper(*args, **kwargs):
        value = func(*args, **kwargs)
        return float(value)
    return wrapper


# I wanted to decouple the adaptar's IO responsibility.
# But these have high potential to be put into a reusable code.
@fix_float
@cache.timed_cache
def exchange_rates_api(from_currency_code: str, to_currency_code: str) -> float:
    """
    Use exchangeratesapi.io to get a rate between two currencies.
    """
    log.debug(f"Requesting rate from {from_currency_code} to "
              f"{to_currency_code}.")
    url = ExchangeRateAPI.prep_conversion_get(
        from_currency_code=from_currency_code,
        to_currency_code=to_currency_code)
    response = requests.get(url).json()
    return ExchangeRateAPI.response2rate(response=response).rate


@fix_float
@cache.timed_cache
def exchangerate_api_com(from_currency_code: str, to_currency_code: str) -> float:
    """
    Use exchangerate-api.com to get a rate between two currencies.
    """
    log.debug(f"Requesting rate from {from_currency_code} to "
              f"{to_currency_code}.")
    url = ExchangeRateAPICom.prep_conversion_get(
        from_currency_code=from_currency_code,
        to_currency_code=to_currency_code)
    response = requests.get(url).json()
    return ExchangeRateAPICom.response2rate(response=response).rate

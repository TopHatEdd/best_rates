"""
Quotes API router.
The API accepts queries from API clients asking to get a quote for
exchanging money from one currency to another.
"""
from typing import Any
import enum
import logging
import decimal
import pydantic
import fastapi
from .. import exchanges
from .. import config

router = fastapi.APIRouter()
log = logging.getLogger(__name__)


class CountryCode(str, enum.Enum):
    USD = "USD"
    EUR = "EUR"
    ILS = "ILS"


class Quote(pydantic.BaseModel):
    """
    Exchange quote in response to client's request.
    """
    exchange_rate: float
    currency_code: str
    amount: int

    def __init__(self, **data: Any):
        super().__init__(**data)
        # Each threads gets a new decimal.Context
        # https://docs.python.org/3/library/decimal.html#working-with-threads
        decimal.getcontext().Emax = decimal.MAX_EMAX
        human_rounded = decimal.Decimal(self.exchange_rate).quantize(
            decimal.Decimal(10) ** -(config.Rates.precision))
        self.exchange_rate = float(human_rounded)


# TODO: Enforce codes from a list of choices using FastAPI so we don't have to.
@router.get("/")
def ask(from_currency_code: CountryCode, amount: int,
        to_currency_code: CountryCode) -> Quote:
    """
    Supported currencies are USD, EUR and ILS.
    The API calculates the total amount expected in the to_currency_code
    according to an
    exchange rate provided by a 3rd party.
    """
    rate1 = exchanges.exchange_rates_api(from_currency_code=from_currency_code,
                                        to_currency_code=to_currency_code)
    rate2 = exchanges.exchangerate_api_com(from_currency_code=from_currency_code,
                                           to_currency_code=to_currency_code)
    rate = max(rate1, rate2)
    converted_amount = amount * rate
    return Quote(exchange_rate=rate,
                 amount=converted_amount,
                 currency_code=to_currency_code)


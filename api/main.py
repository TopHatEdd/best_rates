"""
Exchange rate API mirror.
Caches exchange rates from a 3rd party API.

Main HTTP API settings in which we define our single URI routing.
"""
import logging
import fastapi
from . import routers
from . import config
from . import exchanges

log = logging.getLogger(__name__)
app = fastapi.FastAPI()
app.include_router(routers.quote.router, prefix="/api/quote")


@app.on_event("startup")
async def startup():
    """
    Initialize common resources. Currently, only log.
    """
    init_log(logging.getLogger(), config.All.log_level)
    if config.ExchangeRateAPICom.key == '':
        log.warning("No exchangerate-api.com key set. Disabling it.")
        # Hardcore. Probably will use a flag somewhere in prod.
        exchanges.exchangerate_api_com = lambda *_, **__: -1

    log.debug("FastAPI startup complete")


def init_log(log: logging.Logger, log_level: int, log_boto: bool=False):
    """
    Setup a given logger to output to stdout.
    Enable or disable boto debug logs to stdout.
    """
    out = logging.StreamHandler()
    datefmt = "%Y-%m-%d %H:%M:%S %z"
    # TODO: Align with gunicorn's formatter. Need to yank worker number
    fmt = ("[%(asctime)s] [%(module)s.%(funcName)s] [%(levelname)s] - "
          "%(message)s")
    formatter = logging.Formatter(fmt=fmt, datefmt=datefmt)
    out.setFormatter(formatter)
    out.setLevel(log_level)
    root_log = logging.getLogger()
    root_log.setLevel(logging.DEBUG)

    if log_boto:
        root_log.addHandler(out)  # Will log boto's messages.
    else:
        log.addHandler(out)


"""
Cache DB abstraction with a help wrapper to cache function calls using
said DB.
For more granular control (such as different expiration times for different
functions, need to move away from Singleton.
"""
import abc
import functools
import redis
from . import config


class DB(type, metaclass=abc.ABCMeta):
    _connection = None
    expiration_msec = config.Cache.expiration_msec

    @property
    def connection(cls):
        if cls._connection is None:
            cls._connection = cls.open_connection()
        return cls._connection

    @classmethod
    @abc.abstractmethod
    def open_connection(cls, *args, **kwargs):
        pass

    @classmethod
    @abc.abstractmethod
    def write(cls, key: str, value: str) -> bool:
        pass

    @classmethod
    @abc.abstractmethod
    def read(cls, key: str) -> str:
        pass


class RedisDB(object, metaclass=DB):
    url = config.Cache.url
    encoding = config.Cache.encoding

    @classmethod
    def open_connection(cls, *args, **kwargs):
        return redis.from_url(url=cls.url)

    @classmethod
    def write(cls, key: str, value: str) -> bool:
        key = key.encode(cls.encoding)
        value = value.encode(cls.encoding)
        return cls.connection.set(name=key, value=value, ex=cls.expiration_msec)

    @classmethod
    def read(cls, key: str) -> str:
        key = key.encode(cls.encoding)
        value = cls.connection.get(name=key)
        try:
            return value.decode(cls.encoding)
        except AttributeError:
            return None


def timed_cache(func):
    @functools.wraps(func)
    def wrapped(*args, **kwargs):
        key = (func.__code__.co_name, args, tuple(sorted(kwargs.items())))
        key = str(hash(key))  # Keep using explicit encoding
        cached = RedisDB.read(key)
        if cached is None:
            value = str(func(*args, **kwargs))
            RedisDB.write(key, value)
            cached = value
        return cached
    return wrapped


# Best Rates API
Exchange rates HTTP API. Allows requesting for inter-FIAT conversion rates
and calculate amount exchanged.  
  
Uses the lightning fast APIs and chooses the best rate between them. Caches 
results using `Redis` for each API before responding with a quote.

## Usage
```bash
docker-compose up -d
```
Then access the `OpenAPI` generated docs at
```
http://127.0.0.1/docs
```

## Configurations
Below are the possible ENV variable configurations and their effects. These
are read by `docker-compose` in `extra_config.env` but any other method to
pass in ENV vars should work.
* **CACHE_EXPIRATION_MSEC** - How long to cache results, in miliseconds. Default `10` seconds.
* **REDIS_URL** - Redis URL connection. This allows easy scaling and TLS. Default `redis://redis`.
* **REDIS_ENCODING** - In case the default `UTF-8` isn't good enough. Explicit is better than implicit.
* **RATE_PRECISION** - Determine rate precisions (`3` will result `1.3031 -> 1.303`). Default `3`.
* **EXCHANGERATE_API_COM_KEY** - API key for exchangerate-api.com. Expensive test will fail on this API without a key.
* **LOG_LEVEL** - Log verbosity. Default is DEBUG.

## Test
Note that, by default, heavy tests aren't run. These are "end to end" tests
that include both connecting to a public API and an actual Redis. The `pytest`
marks are
* **cachedb** - Redis tests.
* **expensive** - Public APIs.

Add `-m "not expensive"` to run all non-expensive tests, for example. Or
`-m "cachedb"` to only run `cachedb` marked tests. These flags can be added
to both `poetry` or `docker run`, assuming the external resource is available.

### Using docker-compose
Already has the mark `cachedb` and will execute `Redis` tests.
```bash
docker-compose --file docker-compose-tests.yml
```

### Using poetry
```bash
PYTHONPATH=`pwd` poetry run pytest
```

### Using docker
```bash
docker build -f Dockerfile.tests -t simplex/best_rates:tests-latest
docker run --rm -t simplex/best_rates:tests-latest
```
If you've already built the `docker image` once and the dependencies haven't
changed, one can use `-v ``pwd``/api:/app/api` during ongoing dev to avoid
building new images for each change.

# Extra materials used
Things I had to look up.
* [Dealing with money, use decimal](https://stackoverflow.com/a/3221672/3713120)
  - [Decimal docs](https://docs.python.org/3/library/decimal.html)
* [Mocking requests.Response](https://github.com/psf/requests/blob/master/tests/test_requests.py#L290)
* [FastAPI dir structure](https://fastapi.tiangolo.com/tutorial/bigger-applications/)
* [Redis commands](https://redis.io/commands)
  - Scan and expiration flags for `set`.
* [docker-compose build](https://docs.docker.com/compose/compose-file/compose-file-v3/#build)
* Maybe I'll remember these by my next incarnation.
  - [Log format](https://docs.python.org/3/library/logging.html#logrecord-attributes)
  - [Date format](https://docs.python.org/3/library/time.html#time.strftime)
* [pydantic choices](https://pydantic-docs.helpmanual.io/usage/types/#enums-and-choices)
